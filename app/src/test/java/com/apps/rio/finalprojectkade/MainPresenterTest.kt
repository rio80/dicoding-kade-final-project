package com.apps.rio.finalprojectkade

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.EventResponse
import com.apps.rio.finalprojectkade.presenter.MainPresenter
import com.apps.rio.finalprojectkade.view.MainView
import com.apps.rio.finalprojectkade.utils.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainPresenterTest{
    @Test
    fun getEventPastList(){
        val teams: MutableList<Event> = mutableListOf()
        val response = EventResponse(teams)
        val league = "4328"

        GlobalScope.launch {
            Mockito.`when`(gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getEventsPast(league)).await(),
                    EventResponse::class.java
            )).thenReturn(response)

            presenter.getEventPastList(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showEventList(teams)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getEventNextList(){
        val teams: MutableList<Event> = mutableListOf()
        val response = EventResponse(teams)
        val league = "4328"

        GlobalScope.launch {
            Mockito.`when`(gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getEventsNext(league)).await(),
                    EventResponse::class.java
            )).thenReturn(response)

            presenter.getEventNextList(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showEventList(teams)
            Mockito.verify(view).hideLoading()
        }
    }


    @Mock
    private
    lateinit var view: MainView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: APIRepository

    @Mock
    private lateinit var presenter: MainPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(view, apiRepository, gson, TestContextProvider())

    }
}