package com.apps.rio.finalprojectkade

import com.apps.rio.finalprojectkade.API.APIRepository
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class RequestDataPlayer {

    @Mock
    private var idPlayer: String = "133604"

    @Test
    fun testDoRequest() {
        val apiRepository = Mockito.mock(APIRepository::class.java)
        val url = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php?id=" + idPlayer
        apiRepository.doRequest(url)
        Mockito.verify(apiRepository).doRequest(url)
    }
}