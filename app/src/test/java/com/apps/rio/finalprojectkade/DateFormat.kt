package com.apps.rio.finalprojectkade

import com.apps.rio.finalprojectkade.utils.DateUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import java.text.SimpleDateFormat


class DateFormat {

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }


    //    Fungsi untuk merubah format date menjadi Format GMT
    @Test
    fun testChangeDate() {

        val date = DateUtils.strToDate("2018-02-02")

        val date_list = DateUtils.convertToGMT7("2018-02-02", "05:50:10")

        val testDateEvent = DateUtils.convertFormatDate(date)
        val testTimeEvent = SimpleDateFormat("HH:mm").format(date_list)

//        Test Error
//        Assert.assertEquals("2018-02-02",testDateEvent)
//    Assert.assertEquals("12:20:30",testTimeEvent)


//        Test Passed
        Assert.assertEquals("Fri, 02 Feb 2018", testDateEvent)

//        Hitung Selisih dari parameter jam jadi tambah 7
        Assert.assertEquals("12:50", testTimeEvent)
    }


}