package com.apps.rio.finalprojectkade

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.apps.rio.finalprojectkade.R.id.*
import com.apps.rio.finalprojectkade.activity.MainActivity
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith



@RunWith(AndroidJUnit4::class)
class testRecyclerViewBehaviour(){
    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setup() {
        activityRule.activity.supportFragmentManager.beginTransaction()
        Thread.sleep(500)
    }

    private lateinit var myViewActions: ViewActions
    @Test
    fun testRecyclerViewBehaviour() {
        Espresso.onView(ViewMatchers.withId(R.id.liga_last_layout)).
                        check(ViewAssertions.matches((isDisplayed())))
        Thread.sleep(1000)

        Espresso.onView(ViewMatchers.withId(R.id.list_event)).perform(ViewActions.click())
        Thread.sleep(2000)

        Espresso.onView(AllOf.allOf(ViewMatchers.withId(add_to_favorite),isDisplayed()))
                .perform(ViewActions.click())

        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.detail_match)).perform(ViewActions.pressBack())

        Espresso.onView(ViewMatchers.withId(R.id.nav_team)).perform(ViewActions.click())
        Thread.sleep(2000)

        Espresso.onView(ViewMatchers.withId(R.id.list_team)).perform(ViewActions.click())
        Thread.sleep(2000)

        Espresso.onView(AllOf.allOf(ViewMatchers.withId(add_to_favorite),isDisplayed()))
                .perform(ViewActions.click())

        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.layout_team)).perform(ViewActions.pressBack())
    }
}