package com.apps.rio.finalprojectkade.fragment.favorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.adapter.FavoritePagerAdapter
import kotlinx.android.synthetic.main.fragment_favorite_tabs.view.*
import org.jetbrains.anko.support.v4.find

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FavoriteTabs : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorite_tabs, container, false)
//        ((AppCompatActivity)ac)
//        supportActionBar?.title = "Match Detail"
        view.viewpager_favorite.adapter = FavoritePagerAdapter(childFragmentManager)

        view.tabs_favorite.setupWithViewPager(view.viewpager_favorite)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        (activity as AppCompatActivity).supportActionBar?.title = "Your Favorite"

        val toolbar = find<Toolbar>(R.id.favorite_toolbar)
        toolbar.setSubtitle("Your Favorite")

    }


}
