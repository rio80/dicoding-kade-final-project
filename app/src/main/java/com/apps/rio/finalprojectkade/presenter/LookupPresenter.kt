package com.apps.rio.finalprojectkade.presenter

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.LookupResponse
import com.apps.rio.finalprojectkade.utils.CoroutineContextProvider
import com.apps.rio.finalprojectkade.view.LookupView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LookupPresenter(private val view: LookupView,
                      private val apiRepository: APIRepository,
                      private val gson: Gson,
                      private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeam(idTeamHome: String?, idTeamAway: String?) {
        view.showLoading()
        GlobalScope.launch(context.main) {
            val dataHome =
                gson.fromJson(apiRepository.doRequest(
                        TheSportDBApi.getClubImage(idTeamHome)
                ).await(), LookupResponse::class.java)


            val dataAway =
                gson.fromJson(apiRepository.doRequest(
                        TheSportDBApi.getClubImage(idTeamAway)
                ).await(), LookupResponse::class.java)

            view.hideLoading()
            view.showLookupList(dataHome.teams, dataAway.teams)
        }
    }

}