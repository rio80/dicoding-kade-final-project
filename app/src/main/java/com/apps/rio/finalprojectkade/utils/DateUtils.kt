package com.apps.rio.finalprojectkade.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {
    companion object {
        @SuppressLint("SimpleDateFormat")
        fun convertToGMT7(date: String?, time:String?): Date? {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val dateTime = "$date $time"
            return formatter.parse(dateTime)
        }

        fun convertFormatDate(date: Date?): String? = with(date ?: Date()) {
            SimpleDateFormat("EEE, dd MMM yyy", Locale.ENGLISH).format(this)
        }

        fun  strToDate(strDate: String?, pattern: String= "yyyy-MM-dd"): Date {
            val format = SimpleDateFormat(pattern)
            val date = format.parse(strDate)

            return date
        }
    }
}