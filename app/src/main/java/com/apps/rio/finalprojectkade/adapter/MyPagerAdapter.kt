package com.apps.rio.finalprojectkade.adapter

import android.support.v4.app.FragmentManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import com.apps.rio.finalprojectkade.fragment.match.FragmentLigaLast
import com.apps.rio.finalprojectkade.fragment.match.FragmentLigaNext

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
            FragmentLigaLast(),
            FragmentLigaNext()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position] as Fragment
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Last Match"
            else -> "Next Match"
        }
    }


}