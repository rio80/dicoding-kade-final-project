package com.apps.rio.finalprojectkade.activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.R.drawable.ic_add_to_favorites
import com.apps.rio.finalprojectkade.R.drawable.ic_added_to_favorites
import com.apps.rio.finalprojectkade.R.id.*
import com.apps.rio.finalprojectkade.R.menu.detail_menu
import com.apps.rio.finalprojectkade.anko.ItemDetail
import com.apps.rio.finalprojectkade.helper.database
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.Lookup
import com.apps.rio.finalprojectkade.presenter.LookupPresenter
import com.apps.rio.finalprojectkade.view.LookupView
import com.google.gson.Gson
import org.jetbrains.anko.db.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.setContentView
import java.lang.Exception

class DetailActivity : AppCompatActivity(), LookupView {
    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showLookupList(dataHome: List<Lookup>, dataAway: List<Lookup>) {
        ItemDetail(list,
                dataHome[0].teamBadge.toString(),
                dataAway[0].teamBadge.toString())
                .setContentView(this)


        homeBadge = dataHome[0].teamBadge.toString()
        awayBadge = dataAway[0].teamBadge.toString()
    }


    private lateinit var list: Event


    private lateinit var presenter: LookupPresenter


    private lateinit var homeBadge: String
    private lateinit var awayBadge: String

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent

        try {
            list = intent.getParcelableExtra<Event>("position")
        } catch (ex: Exception) {

        }

        favoriteState()
        val gson = Gson()
        val request = APIRepository()
        presenter = LookupPresenter(this, request, gson)
        presenter.getTeam(list.idHomeTeam, list.idAwayTeam)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Match Detail"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(Event.TABLE_FAVORITE,
                        Event.ID_EVENT to list.idEvent,
                        Event.ID_TEAM_HOME to list.idHomeTeam,
                        Event.ID_TEAM_AWAY to list.idAwayTeam,
                        Event.SCORE_HOME to list.intHomeScore,
                        Event.SCORE_AWAY to list.intAwayScore,
                        Event.DATE_EVENT to list.dateEvent,
                        Event.TIME_EVENT to list.strTime,
                        Event.SPORT_NAME to list.strSport,
                        Event.HOME_TEAM to list.strHomeTeam,
                        Event.AWAY_TEAM to list.strAwayTeam,
                        Event.AWAY_GOAL to list.strAwayGoalDetails,
                        Event.HOME_GOAL to list.strHomeGoalDetails,
                        Event.HOME_SHOT to list.intHomeShots,
                        Event.AWAY_SHOT to list.intAwayShots,
                        Event.HOME_KEEPER to list.strHomeGoalDetails,
                        Event.AWAY_KEEPER to list.strAwayGoalDetails,
                        Event.HOME_DEFENSE to list.strHomeLineupDefense,
                        Event.AWAY_DEFENSE to list.strAwayLineupDefense,
                        Event.HOME_MIDFIELD to list.strHomeLineupMidfield,
                        Event.AWAY_MIDFIELD to list.strAwayLineupMidfield,
                        Event.HOME_FORWARD to list.strHomeLineupForward,
                        Event.AWAY_FORWARD to list.strAwayLineupForward,
                        Event.HOME_SUBSTITUTE to list.strHomeLineupSubstitutes,
                        Event.AWAY_SUBSTITUTE to list.strAwayLineupSubstitutes)
            }
            snackbar(window.decorView.rootView,"Added to Favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(window.decorView.rootView, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(Event.TABLE_FAVORITE, "(ID_TEAM_HOME = {id})",
                        "id" to list.idHomeTeam.toString())
            }
            snackbar(window.decorView.rootView, "Removed to Favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(window.decorView.rootView, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    private fun favoriteState() {
        database.use {
            val result = select(Event.TABLE_FAVORITE)
                    .whereArgs("(ID_TEAM_HOME = {id})",
                            "id" to list.idHomeTeam.toString())
            val Event = result.parseList(classParser<Event>())
            if (!Event.isEmpty()) isFavorite = true
        }
    }
}