package com.apps.rio.finalprojectkade.activity

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.helper.database
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.Player
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.toast

class PlayerDetailActivity : AppCompatActivity() {

    private lateinit var players:Player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)

        supportActionBar?.title="Player Detail"

        try {
            players = intent.getParcelableExtra<Player>("player_intent")

            if (players.playerFanart1 === null || players.playerFanart1 === ""){
                Picasso.get().load(R.drawable.null_image).into(image_fanart1)
            }else{
                Picasso.get().load(players.playerFanart1).into(image_fanart1)
            }

            if (players.playerWeight == "") players.playerWeight = "0"
            player_weight.text = players.playerWeight
            if (players.playerHeight == "") players.playerHeight = "0"
            player_height.text = players.playerHeight
            position_player.text = players.playerPosition
            descriptionEN.text = players.descriptionEN

        } catch (ex: Exception) {
            toast(ex.message.toString())
        }
    }


}
