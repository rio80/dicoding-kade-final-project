package com.apps.rio.finalprojectkade.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.apps.rio.finalprojectkade.R.id.*
import com.apps.rio.finalprojectkade.anko.ItemList
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.utils.DateUtils
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import java.text.SimpleDateFormat

class ListAdapter(private val events: List<Event>,
                  private val listener: (Event) -> Unit)
    : RecyclerView.Adapter<ListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ViewHolder {
        return ViewHolder(ItemList().createView(AnkoContext.create(parent.context, parent)))

    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(events[position], listener)
        holder.setIsRecyclable(false)
    }

    class ViewHolder(override val containerView: View)
        : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private var dateEvent: TextView = containerView.find(date_event)
        private var timeEvent: TextView = containerView.find(time_event)
        private var homeTeam: TextView = containerView.find(home_team)
        private var awayTeam: TextView = containerView.find(away_team)
        private var tengahVS: TextView = containerView.find(tengah)
        private var homeScore: TextView = containerView.find(home_score)
        private var awayScore: TextView = containerView.find(away_score)

        fun bindItem(events: Event, listener: (Event) -> Unit) {

//            dateEvent.text = events.dateEvent
            val date_list = DateUtils.convertToGMT7(events.dateEvent, events.strTime)
            val date = DateUtils.strToDate(events.dateEvent)
            dateEvent.text = DateUtils.convertFormatDate(date)
            timeEvent.text = SimpleDateFormat("HH:mm").format(date_list)
            homeTeam.text = events.strHomeTeam

            awayTeam.text = events.strAwayTeam
            tengahVS.text = " vs "
            homeScore.text = events.intHomeScore
            awayScore.text = events.intAwayScore


            containerView.setOnClickListener {
                listener(events)
            }
        }
    }
}