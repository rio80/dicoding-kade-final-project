package com.apps.rio.finalprojectkade.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.anko.ItemList
import com.apps.rio.finalprojectkade.model.Event
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class SearchMatchListAdapter(private val events: List<Event>, private val listener: (Event) -> Unit):
        RecyclerView.Adapter<SearchMatchListAdapter.EventViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            EventViewHolder(ItemList().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount() = events.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) = holder.bindItem(events[position], listener)

    class EventViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val eventDate: TextView = itemView.find(R.id.date_event)
        private val eventTime: TextView = itemView.find(R.id.time_event)
        private val homeTeam: TextView = itemView.find(R.id.home_team)
        private var tengahVS: TextView = itemView.find(R.id.tengah)
        private val homeScore: TextView = itemView.find(R.id.home_score)
        private val awayTeam: TextView = itemView.find(R.id.away_team)
        private val awayScore: TextView = itemView.find(R.id.away_score)

        @SuppressLint("SimpleDateFormat")
        fun bindItem(event: Event, listener: (Event) -> Unit){
            val date_list = toGMTFormat(event.dateEvent, event.strTime)
            val date = strTodate(event.dateEvent)
            eventDate.text = changeFormatDate(date)
            eventTime.text = SimpleDateFormat("HH:mm").format(date_list)

            homeTeam.text = event.strHomeTeam
            homeScore.text = event.intHomeScore
            tengahVS.text = " vs "
            awayTeam.text = event.strAwayTeam
            awayScore.text = event.intAwayScore

            itemView.onClick { listener(event) }
        }
        @SuppressLint("SimpleDateForamt")
        fun toGMTFormat(date: String?, time:String?): Date? {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val dateTime = "$date $time"
            return formatter.parse(dateTime)
        }

        fun changeFormatDate(date: Date?): String? = with(date ?: Date()) {
            SimpleDateFormat("EEE, dd MMM yyy", Locale.ENGLISH).format(this)
        }

        fun  strTodate(strDate: String?, pattern: String= "yyyy-MM-dd"): Date {
            val format = SimpleDateFormat(pattern)
            val date = format.parse(strDate)

            return date
        }
    }
}