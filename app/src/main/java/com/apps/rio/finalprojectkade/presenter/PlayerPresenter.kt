package com.apps.rio.finalprojectkade.presenter

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.PlayerResponse
import com.apps.rio.finalprojectkade.utils.CoroutineContextProvider
import com.apps.rio.finalprojectkade.view.PlayerView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PlayerPresenter(private val view: PlayerView,
                    private val apiRepository: APIRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getPlayer(leagueId: String?) {
        view.showLoading()
        GlobalScope.launch(context.main) {
            val data = gson.fromJson(apiRepository.doRequest(
                    TheSportDBApi.getPlayer(leagueId)
            ).await(), PlayerResponse::class.java)

            view.hideLoading()
            view.showPlayer(data.player!!)

        }
    }
}