package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.League
import com.apps.rio.finalprojectkade.model.Team

interface TeamView {
    fun showLoading()
    fun hideLoading()
    fun showLeagueList(data: List<League>)
    fun showTeamList(data: List<Team>)
}