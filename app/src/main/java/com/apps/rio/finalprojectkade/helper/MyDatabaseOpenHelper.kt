package com.apps.rio.finalprojectkade.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.Team
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteTeam.db", null, 1) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable(Event.TABLE_FAVORITE, true,
                Event.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Event.ID_EVENT to TEXT,
                Event.DATE_EVENT to TEXT,
                Event.TIME_EVENT to TEXT,
                Event.SPORT_NAME to TEXT,
                Event.ID_TEAM_HOME to TEXT,
                Event.HOME_TEAM to TEXT,
                Event.SCORE_HOME to TEXT,
                Event.HOME_GOAL to TEXT,
                Event.HOME_SHOT to TEXT,
                Event.HOME_KEEPER to TEXT,
                Event.HOME_DEFENSE to TEXT,
                Event.HOME_MIDFIELD to TEXT,
                Event.HOME_FORWARD to TEXT,
                Event.HOME_SUBSTITUTE to TEXT,

                Event.ID_TEAM_AWAY to TEXT,
                Event.AWAY_TEAM to TEXT,
                Event.SCORE_AWAY to TEXT,
                Event.AWAY_GOAL to TEXT,
                Event.AWAY_SHOT to TEXT,
                Event.AWAY_KEEPER to TEXT,
                Event.AWAY_DEFENSE to TEXT,
                Event.AWAY_MIDFIELD to TEXT,
                Event.AWAY_FORWARD to TEXT,
                Event.AWAY_SUBSTITUTE to TEXT)

        db.createTable(Team.TABLE_TEAM, true,
                Team.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Team.ID_TEAM to TEXT,
                Team.TEAM_NAME to TEXT,
                Team.TEAM_BADGE to TEXT,
                Team.FORMED_YEAR to TEXT,
                Team.STADIUM_NAME to TEXT,
                Team.DESCRIPTION_EN to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable(Event.TABLE_FAVORITE, true)
        db.dropTable(Team.TABLE_TEAM, true)
    }
}

// Access property for Context
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)