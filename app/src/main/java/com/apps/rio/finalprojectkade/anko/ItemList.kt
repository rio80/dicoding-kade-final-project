package com.apps.rio.finalprojectkade.anko

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.apps.rio.finalprojectkade.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class ItemList : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            relativeLayout {
                cardView {
                    layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT).apply {
                        leftMargin = dip(10)
                        rightMargin = dip(10)
                        topMargin = dip(5)
                        bottomMargin = dip(5)
                    }
                    background.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
//                    backgroundColor = Color.WHITE
                    radius = dip(6).toFloat()
                    cardElevation = dip(2).toFloat()
                    relativeLayout {
                        lparams(width = matchParent, height = matchParent)
                        textView {
                            id = R.id.date_event
//                    hint = "date event"
                            textColor = Color.parseColor("#ffae00")
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            bottomMargin = dip(10)
                        }
                        textView {
                            id = R.id.time_event
//                    hint = "time event"
                            textColor = Color.parseColor("#ffae00")
                        }.lparams(width = wrapContent, height = wrapContent) {
                            below(R.id.date_event)
                            centerHorizontally()
                            bottomMargin = dip(10)
                        }

                        textView {
                            id = R.id.home_team
                            textSize = sp(6).toFloat()
//                    hint = "team home"
                            textAlignment = View.TEXT_ALIGNMENT_VIEW_END
                        }.lparams(width = wrapContent, height = wrapContent) {
                            below(R.id.time_event)
                            rightMargin = dip(26)
                            leftOf(R.id.home_score)
                            alignParentLeft()
                        }
                        textView {
                            id = R.id.home_score
                            textSize = sp(6).toFloat()
//                    hint = "score home"
                            setTypeface(null, Typeface.BOLD)
                            textAlignment = View.TEXT_ALIGNMENT_VIEW_END
                        }.lparams(width = wrapContent, height = wrapContent) {
                            below(R.id.time_event)
                            leftOf(R.id.tengah)
                            centerInParent()
                            centerHorizontally()

                        }
                        textView {
                            id = R.id.tengah
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerInParent()
                            below(R.id.time_event)
                        }
                        textView {
                            id = R.id.away_score
                            textSize = sp(6).toFloat()
//                    hint = "away score"
                            setTypeface(null, Typeface.BOLD)
                        }.lparams(width = wrapContent, height = wrapContent) {
                            below(R.id.time_event)
                            rightOf(R.id.tengah)
                            centerInParent()
                            centerHorizontally()
                        }
                        textView {
                            id = R.id.away_team
                            textSize = sp(6).toFloat()
//                    hint = "away team"
                        }.lparams(width = wrapContent, height = wrapContent) {
                            below(R.id.time_event)
                            leftMargin = dip(26)
                            rightOf(R.id.away_score)
                            alignParentRight()
                        }
                    }
                }
            }
        }
    }

}