package com.apps.rio.finalprojectkade.anko

import android.view.View
import android.widget.LinearLayout
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.activity.DetailActivity
import com.apps.rio.finalprojectkade.model.Event
import com.bumptech.glide.Glide
import org.jetbrains.anko.*

class ItemDetail(var list: Event, var listTeamHome: String, var listTeamAway: String) : AnkoComponent<DetailActivity> {
    override fun createView(ui: AnkoContext<DetailActivity>) = with(ui) {
        linearLayout {
            id = R.id.detail_match
            orientation = LinearLayout.VERTICAL
            scrollView {
                relativeLayout {
                    imageView {
                        id = R.id.img_club_home
                        padding = dip(10)
                        Glide.with(this).load(listTeamHome).into(this)
                    }.lparams(width = dip(120), height = dip(100)) {
                        alignParentLeft()
                        leftMargin = dip(10)
                        topMargin = dip(30)
                    }
                    textView {
                        hint = "ini date event"
                        id = R.id.date_event
                        textColor = 0xef9b00.opaque
                        textSize = 15f
                        text = list.dateEvent
                    }.lparams(width = wrapContent, height = wrapContent) {
                        centerHorizontally()
                        topMargin = dip(14)
                        bottomMargin = dip(14)
                    }
                    imageView {
                        id = R.id.img_club_away
                        padding = dip(10)
                        Glide.with(this).load(listTeamAway).into(this)

                    }.lparams(width = dip(120), height = dip(100)) {
                        alignParentRight()
                        topMargin = dip(30)
                        rightMargin = dip(10)
                    }
                    textView {
                        id = R.id.score_home
                        textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        textColor = 0x000.opaque
                        textSize = 18f
                        text = list.intHomeScore
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.date_event)
                        rightOf(R.id.img_club_home)
                        leftOf(R.id.tengah)
                        centerInParent()
                        centerHorizontally()
                    }
                    textView(" vs ") {
                        id = R.id.tengah
                        padding = dip(4)
                        textColor = 0x000.opaque
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.date_event)
                        centerInParent()
                    }
                    textView {
                        id = R.id.score_away
                        textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        textColor = 0x000.opaque
                        textSize = 18f
                        text = list.intAwayScore
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.date_event)
                        rightOf(R.id.tengah)
                        leftOf(R.id.img_club_away)
                        centerInParent()
                        centerHorizontally()
                    }
                    textView {
                        id = R.id.home_team
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                        textColor = 0xef9b00.opaque
                        textSize = 20f
                        text = list.strHomeTeam
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.img_club_home)
                        leftOf(R.id.tengah)
                        alignParentLeft()
                        leftMargin = dip(10)
                    }
                    textView {
                        id = R.id.away_team
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                        textColor = 0xef9b00.opaque
                        textSize = 20f
                        text = list.strAwayTeam
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.img_club_away)
                        rightOf(R.id.tengah)
                        alignParentRight()
                        rightMargin = dip(10)
                    }
                    view {
                        backgroundColor = 0x000.opaque
                        id = R.id.garis1
                    }.lparams(width = matchParent, height = dip(1)) {
                        below(R.id.away_team)
                        leftMargin = dip(10)
                        topMargin = dip(16)
                        rightMargin = dip(10)
                        bottomMargin = dip(16)
                    }
                    relativeLayout {
                        id = R.id.layout_goals_detail
                        textView {
//                            hint = "ini adalah goal detaail ya kawan kawan"
                            id = R.id.goals_home_detail
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            textColor = 0x000.opaque
                            text = list.strHomeGoalDetails
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_goals_detail)
                            leftMargin = dip(10)
                            bottomMargin = dip(16)
                        }
                        textView("Goals") {
                            id = R.id.title_goals_detail
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
//                            hint = "ini adalah goal detaail ya kawan kawan"
                            id = R.id.goals_away_detail
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            textColor = 0x000.opaque
                            text = list.strAwayGoalDetails
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_goals_detail)
                            alignParentRight()
                            rightMargin = dip(10)
                            bottomMargin = dip(16)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.garis1)
                    }
                    relativeLayout {
                        id = R.id.layout_shots_detail

                        textView {
                            id = R.id.shots_home
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.intHomeShots
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_shots)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Shots") {
                            id = R.id.title_shots
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.shots_away
                            text = list.intAwayShots
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_shots)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.layout_goals_detail)
                    }
                    view {
                        backgroundColor = 0x000.opaque
                        id = R.id.garis2
                    }.lparams(width = matchParent, height = dip(1)) {
                        below(R.id.layout_shots_detail)
                        leftMargin = dip(10)
                        topMargin = dip(12)
                        rightMargin = dip(10)
                        bottomMargin = dip(6)
                    }
                    textView("Line Up") {
                        id = R.id.LineUp
                        textSize = 16f
                    }.lparams(width = wrapContent, height = wrapContent) {
                        below(R.id.garis2)
                        centerHorizontally()
                    }
                    relativeLayout {
                        id = R.id.layout_goals_keeper

                        textView {
                            id = R.id.goal_home_keeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.strHomeLineupGoalkeeper
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_goal_keeper)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Goal Keeper") {
                            id = R.id.title_goal_keeper
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.goal_away_keeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            text = list.strAwayLineupGoalkeeper
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_goal_keeper)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.LineUp)
                        topMargin = dip(10)
                    }
                    relativeLayout {
                        id = R.id.layout_defense

                        textView {
                            id = R.id.home_defense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.strHomeLineupDefense
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_defense)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Defense") {
                            id = R.id.title_defense
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.away_defense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            text = list.strAwayLineupDefense
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_defense)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.layout_goals_keeper)
                        topMargin = dip(14)
                    }
                    relativeLayout {
                        id = R.id.layout_midfield

                        textView {
                            id = R.id.home_midfield
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.strHomeLineupMidfield
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_midfield)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Midfield") {
                            id = R.id.title_midfield
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.away_midfield
                            text = list.strAwayLineupMidfield
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_midfield)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.layout_defense)
                        topMargin = dip(14)
                    }
                    relativeLayout {
                        id = R.id.layout_forward

                        textView {
                            id = R.id.home_forward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.strHomeLineupForward
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_forward)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Forward") {
                            id = R.id.title_forward
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.away_forward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            text = list.strAwayLineupForward
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_forward)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.layout_midfield)
                        topMargin = dip(14)
                    }
                    relativeLayout {
                        id = R.id.layout_substitutes

                        textView {
                            id = R.id.home_substitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            text = list.strHomeLineupSubstitutes
                        }.lparams(width = wrapContent, height = wrapContent) {
                            leftOf(R.id.title_substitutes)
                            alignParentLeft()
                            leftMargin = dip(10)
                        }
                        textView("Substitutes") {
                            id = R.id.title_substitutes
                            textColor = 0xef9b00.opaque
                        }.lparams(width = wrapContent, height = wrapContent) {
                            centerHorizontally()
                            leftMargin = dip(14)
                            rightMargin = dip(14)
                        }
                        textView {
                            id = R.id.away_subsitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            text = list.strAwayLineupSubstitutes
                        }.lparams(width = wrapContent, height = wrapContent) {
                            rightOf(R.id.title_substitutes)
                            alignParentRight()
                            rightMargin = dip(10)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        below(R.id.layout_forward)
                        topMargin = dip(14)
                    }
                }.lparams(width = matchParent, height = matchParent)
            }.lparams(width = matchParent, height = matchParent)
        }

    }
}