package com.apps.rio.finalprojectkade.fragment.match


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.R.id.search_menu
import com.apps.rio.finalprojectkade.activity.SearchMatchActivity
import com.apps.rio.finalprojectkade.adapter.MyPagerAdapter
import kotlinx.android.synthetic.main.fragment_match_tab.view.*
import org.jetbrains.anko.support.v4.find


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentMatchTab : Fragment() {

    private var menuItem: Menu? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_match_tab, container, false)

        view.viewpager_match.adapter = MyPagerAdapter(childFragmentManager)

        view.tabs_match.setupWithViewPager(view.viewpager_match)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = "Match"

        val toolbar = find<Toolbar>(R.id.matches_toolbar)
        toolbar.inflateMenu(R.menu.search)
        toolbar.setOnMenuItemClickListener {
            when(it.itemId){
                search_menu -> {
                    val intent = Intent(context, SearchMatchActivity::class.java)
                    startActivity(intent)
                }
            }

            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            search_menu -> {
                val intent = Intent(context, SearchMatchActivity::class.java)
                startActivity(intent)
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

}
