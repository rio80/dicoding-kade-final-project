package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.Player

interface PlayerView{
    fun showLoading()
    fun hideLoading()
    fun showPlayer(data: List<Player>)
}