package com.apps.rio.finalprojectkade.model

data class EventSearch (
        val event : List<Event>
)