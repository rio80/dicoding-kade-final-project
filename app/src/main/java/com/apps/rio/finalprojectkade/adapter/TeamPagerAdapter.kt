package com.apps.rio.finalprojectkade.adapter;

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.apps.rio.finalprojectkade.fragment.player.FragmentPlayer
import com.apps.rio.finalprojectkade.fragment.team.OverviewFragment

class TeamPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
            OverviewFragment(),
            FragmentPlayer()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position] as Fragment
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Overview"
            else -> "Player"
        }
    }
}
