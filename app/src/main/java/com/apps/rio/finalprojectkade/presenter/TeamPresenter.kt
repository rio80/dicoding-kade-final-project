package com.apps.rio.finalprojectkade.presenter

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.LeagueResponse
import com.apps.rio.finalprojectkade.model.TeamResponse
import com.apps.rio.finalprojectkade.utils.CoroutineContextProvider
import com.apps.rio.finalprojectkade.view.TeamView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class TeamPresenter(private val view: TeamView,
                    private val apiRespository: APIRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getLeagueList() {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRespository
                    .doRequest(TheSportDBApi.getListLeague()).await(),
                    LeagueResponse::class.java

            )

            view.hideLoading()
            view.showLeagueList(data.countrys)

        }
    }

    fun getTeamList(league: String?) {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRespository
                    .doRequest(TheSportDBApi.getTeams(league)).await(),
                    TeamResponse::class.java

            )

            view.hideLoading()
            view.showTeamList(data.teams)

        }
    }

    fun getSearchTeamList(league: String?) {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRespository
                    .doRequest(TheSportDBApi.getSearchTeams(league)).await(),
                    TeamResponse::class.java

            )

            view.hideLoading()
            if (data.teams !== null)
            view.showTeamList(data.teams)

        }
    }

    fun getLookupTeam(league: String?) {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRespository
                    .doRequest(TheSportDBApi.getTeamDetail(league)).await(),
                    TeamResponse::class.java

            )

            view.hideLoading()
            if (data.teams !== null)
                view.showTeamList(data.teams)

        }
    }

}