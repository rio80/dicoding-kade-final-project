package com.apps.rio.finalprojectkade.fragment.favorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.rio.finalprojectkade.R.color.colorAccent

import com.apps.rio.finalprojectkade.activity.DetailActivity
import com.apps.rio.finalprojectkade.adapter.FavoriteAdapter
import com.apps.rio.finalprojectkade.helper.database
import com.apps.rio.finalprojectkade.model.Event
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentFavorite : Fragment() {

    private var favorites: MutableList<Event> = mutableListOf()
    private lateinit var listEvent: RecyclerView
    private lateinit var adapter: FavoriteAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return UI {
            verticalLayout {
                bottomPadding = dip(50)

                relativeLayout {
                    lparams(width = matchParent, height = matchParent)
                    swipeRefresh = swipeRefreshLayout {
                        setColorSchemeResources(colorAccent,
                                android.R.color.holo_green_light,
                                android.R.color.holo_orange_light,
                                android.R.color.holo_red_light)

                        listEvent = recyclerView {
                            lparams(width = matchParent, height = wrapContent)
                            layoutManager = LinearLayoutManager(ctx)
                        }
                    }
                }
            }
        }.view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        favorites.clear()
        adapter = FavoriteAdapter(favorites) {
            context!!.startActivity<DetailActivity>("position" to it)
        }
        listEvent.adapter = adapter
        showFavorite()

        swipeRefresh.onRefresh {
            favorites.clear()
            showFavorite()
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(Event.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<Event>())
            Log.d("favorite_list",result.parseList(classParser<Event>()).toString())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }


}
