package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.Event

interface SearchMatchView{
    fun showLoading()
    fun hideLoading()
    fun showList(data: List<Event>?)
}