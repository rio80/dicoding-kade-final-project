package com.apps.rio.finalprojectkade.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lookup(
        @SerializedName("strTeamBadge")
        var teamBadge:String ? = null
):Parcelable