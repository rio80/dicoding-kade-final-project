package com.apps.rio.finalprojectkade.fragment.player


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.activity.PlayerDetailActivity
import com.apps.rio.finalprojectkade.activity.TeamDetailActivity
import com.apps.rio.finalprojectkade.adapter.PlayerAdapter
import com.apps.rio.finalprojectkade.model.Player
import com.apps.rio.finalprojectkade.presenter.PlayerPresenter
import com.apps.rio.finalprojectkade.utils.invisible
import com.apps.rio.finalprojectkade.utils.visible
import com.apps.rio.finalprojectkade.view.PlayerView
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentPlayer : Fragment(), AnkoComponent<Context>, PlayerView {


    private lateinit var listPlayer: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    private var players: MutableList<Player> = mutableListOf()
    private lateinit var presenter: PlayerPresenter
    private lateinit var adapter: PlayerAdapter
    private lateinit var playerName: String

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showPlayer(data: List<Player>) {
        players.clear()
        players.addAll(data)
        adapter.notifyDataSetChanged()
        listPlayer.scrollToPosition(0)
    }

    private lateinit var teamId: String

    companion object {
        fun newFragment(teamId: String): FragmentPlayer {
            val fragment = FragmentPlayer()
            fragment.teamId = teamId

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return createView(AnkoContext.create(context!!))

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val idTeam = TeamDetailActivity.idTeam


        adapter = PlayerAdapter(players) {
            ctx.startActivity<PlayerDetailActivity>("player_intent" to it)
        }
        listPlayer.adapter = adapter

        val request = APIRepository()
        val gson = Gson()
        presenter = PlayerPresenter(this, request, gson)
        presenter.getPlayer(idTeam)

        swipeRefresh.onRefresh {
            players.clear()
            presenter.getPlayer(idTeam)
            swipeRefresh.isRefreshing = false

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listPlayer = recyclerView {
                        id = R.id.list_player
                        lparams(width = matchParent, height = matchParent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerHorizontally()
                    }
                }
            }
        }
    }

}
