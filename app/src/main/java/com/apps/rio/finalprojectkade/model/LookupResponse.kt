package com.apps.rio.finalprojectkade.model

data class LookupResponse (
        val teams : List<Lookup>
)