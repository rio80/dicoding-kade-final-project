package com.apps.rio.finalprojectkade.presenter

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.EventSearch
import com.apps.rio.finalprojectkade.utils.CoroutineContextProvider
import com.apps.rio.finalprojectkade.view.SearchMatchView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SearchMatchPresenter(private val view: SearchMatchView,
                                 private val apiRepository: APIRepository,
                                 private val gson: Gson,
                                 private val context: CoroutineContextProvider = CoroutineContextProvider()){

    fun getList(query: String?){
        view.showLoading()

        val api = TheSportDBApi.getSearchEvent(query)

        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                    .doRequest(api).await(),
                    EventSearch::class.java)

            view.hideLoading()
            view.showList(data?.event)
        }
    }
}