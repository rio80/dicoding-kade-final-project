package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.Lookup

interface LookupView {
    fun showLoading()
    fun hideLoading()
    fun showLookupList(dataHome: List<Lookup>, dataAway:List<Lookup>)
}