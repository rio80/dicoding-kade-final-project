package com.apps.rio.finalprojectkade.model

data class EventResponse (
        val events : List<Event>
)