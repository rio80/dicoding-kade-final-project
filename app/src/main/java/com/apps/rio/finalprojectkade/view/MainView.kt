package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.League

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data: List<Event>)
    fun showLeagueList(data: List<League>)
}