package com.apps.rio.finalprojectkade.utils

import kotlinx.coroutines.Dispatchers

open class CoroutineContextProvider {
    open val main: kotlin.coroutines.CoroutineContext by lazy { Dispatchers.Main }
}