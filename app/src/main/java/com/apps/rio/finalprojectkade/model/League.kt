package com.apps.rio.finalprojectkade.model;

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class League(
        @SerializedName("idLeague")
        val idLeague: String?,
        @SerializedName("strLeague")
        val strLeague: String?
) : Parcelable {
    override fun toString(): String {
        return strLeague.orEmpty()
    }
    companion object {
        const val TABLE_LEAGUE_FAVORITE : String= "TABLE_LEAGUE_FAVORITE"
        const val ID : String= "ID_"
        const val ID_LEAGUE : String= "ID_LEAGUE"
        const val STR_LEAGUE: String = "STR_LEAGUE"
        const val STR_LEAGUE_ALTERNATE: String = "STR_LEAGUE_ALTERNATE"

    }
}

