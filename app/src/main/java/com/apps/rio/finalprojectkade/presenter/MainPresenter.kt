package com.apps.rio.finalprojectkade.presenter

import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.API.TheSportDBApi
import com.apps.rio.finalprojectkade.model.EventResponse
import com.apps.rio.finalprojectkade.model.LeagueResponse
import com.apps.rio.finalprojectkade.utils.CoroutineContextProvider
import com.apps.rio.finalprojectkade.view.MainView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainPresenter(private val view: MainView,
                    private val apiRepository: APIRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getEventPastList(leagueId: String?) {
        view.showLoading()
        GlobalScope.launch(context.main) {
            val data = gson.fromJson(apiRepository.doRequest(
                    TheSportDBApi.getEventsPast(leagueId)
            ).await(), EventResponse::class.java)

            view.hideLoading()
            view.showEventList(data.events!!)

        }
    }

    fun getEventNextList(leagueId: String?) {
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(apiRepository.doRequest(
                    TheSportDBApi.getEventsNext(leagueId)
            ).await(), EventResponse::class.java)


            view.hideLoading()
            if (data.events == null) else
            view.showEventList(data.events)

        }
    }

    fun getLeagueList() {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getListLeague()).await(),
                    LeagueResponse::class.java)

            view.hideLoading()
            view.showLeagueList(data.countrys)

        }
    }
}