package com.apps.rio.finalprojectkade.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.model.Player
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk27.coroutines.onClick
import android.view.LayoutInflater


class PlayerAdapter(private val players: List<Player>, private val listener: (Player) -> Unit)
    : RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        val mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_fragment_player, parent, false)
        return PlayerViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(players[position], listener)
    }

    class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val playerFanart: ImageView = view.find(R.id.image_player)
        private val playerName: TextView = view.find(R.id.player_name)
        private val playerPosition: TextView = view.find(R.id.player_position)


        fun bindItem(players: Player, listener: (Player) -> Unit) {
            Picasso.get().load(players.playerFanart1).into(playerFanart)
            playerName.text = players.playerName
            playerPosition.text = players.playerPosition

            itemView.onClick { listener(players) }
        }
    }
}