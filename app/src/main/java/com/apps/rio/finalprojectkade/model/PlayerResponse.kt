package com.apps.rio.finalprojectkade.model

data class PlayerResponse (
        val player : List<Player>
)