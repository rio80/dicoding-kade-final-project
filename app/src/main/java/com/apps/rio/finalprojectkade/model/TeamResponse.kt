package com.apps.rio.finalprojectkade.model

data class TeamResponse(
        val teams: List<Team>
)