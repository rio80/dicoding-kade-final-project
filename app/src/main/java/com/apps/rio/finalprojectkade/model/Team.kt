package com.apps.rio.finalprojectkade.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Team(
        var id: Long?,
        @SerializedName("idTeam")
        var teamId: String? = null,

        @SerializedName("strTeam")
        var teamName: String? = null,

        @SerializedName("strTeamBadge")
        var teamBadge: String? = null,

        @SerializedName("intFormedYear")
        var teamFormedYear: String? = null,

        @SerializedName("strStadium")
        var teamStadium: String? = null,

        @SerializedName("strDescriptionEN")
        var teamDescription: String? = null
):Parcelable {
        companion object {
                const val TABLE_TEAM : String= "TABLE_TEAM"
                const val ID : String= "ID_"
                const val ID_TEAM : String= "ID_TEAM"
                const val TEAM_NAME : String= "TEAM_NAME"
                const val TEAM_BADGE: String = "TEAM_BADGE"
                const val FORMED_YEAR: String = "FORMED_YEAR"
                const val STADIUM_NAME: String = "STADIUM_NAME"
                const val DESCRIPTION_EN: String = "DESCRIPTION_EN"
        }
}