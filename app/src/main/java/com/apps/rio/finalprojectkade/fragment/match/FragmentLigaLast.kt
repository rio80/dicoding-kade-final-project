package com.apps.rio.finalprojectkade.fragment.match


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.activity.DetailActivity
import com.apps.rio.finalprojectkade.adapter.ListAdapter
import com.apps.rio.finalprojectkade.utils.invisible
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.model.League
import com.apps.rio.finalprojectkade.presenter.MainPresenter
import com.apps.rio.finalprojectkade.view.MainView
import com.apps.rio.finalprojectkade.utils.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentLigaLast : Fragment(), MainView {

    private var leagues: MutableList<League> = mutableListOf()
    private var events: MutableList<Event> = mutableListOf()
    private lateinit var listEvent: RecyclerView
    private lateinit var adapter: ListAdapter
    private lateinit var presenter: MainPresenter
    private lateinit var progressBar: ProgressBar
    private lateinit var spinner: Spinner
    private var leagueId = "4328"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = ListAdapter(events) {
            context!!.startActivity<DetailActivity>("position" to it)
        }

        listEvent.adapter = adapter

        val request = APIRepository()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)
        presenter.getEventPastList("4328")
        presenter.getLeagueList()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(context!!))
    }

    fun createView(ui: AnkoContext<Context>) = with(ui) {
        verticalLayout {
            id = R.id.liga_last_layout
            bottomPadding = dip(50)

            spinner = spinner {
                id = R.id.spinner_match

                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val liga = spinner.selectedItem as League
                        leagueId = liga.idLeague.orEmpty()
                        if (leagueId.isNotEmpty()) {
                            presenter.getEventPastList(leagueId)
                        }
                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                }
            }

            relativeLayout {
                lparams(width = matchParent, height = matchParent)
                listEvent = recyclerView {
                    id = R.id.list_event
                    lparams(matchParent, wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }

                progressBar = progressBar {
                }.lparams {
                    centerHorizontally()
                }
            }
        }
    }


    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showEventList(data: List<Event>) {
        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()
        listEvent.scrollToPosition(0)
    }

    override fun showLeagueList(data: List<League>) {
        hideLoading()
        leagues.clear()
        leagues.addAll(data)

        val spinnerAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, leagues)
        spinner.adapter = spinnerAdapter

        spinner.setSelection(spinnerAdapter.getPosition(League(
                "4328",
                "English Premier League")))
    }


}
