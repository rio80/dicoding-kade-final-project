package com.apps.rio.finalprojectkade.activity


import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.R.id.add_to_favorite
import com.apps.rio.finalprojectkade.adapter.TeamPagerAdapter
import com.apps.rio.finalprojectkade.helper.database
import com.apps.rio.finalprojectkade.model.Team
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find

class TeamDetailActivity : AppCompatActivity() {


    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    companion object {
        lateinit var idTeam:String
        lateinit var overview:String
    }

    private lateinit var team: Team

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_team_detail)
        supportActionBar?.hide()

        val toolbar = find<Toolbar>(R.id.toolbar)
        toolbar.setSubtitle("Team Detail")
        toolbar.inflateMenu(R.menu.detail_menu)

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                add_to_favorite -> {
                    if (isFavorite) removeFromFavorite() else addToFavorite()
                    isFavorite = !isFavorite
                    setFavorite()
                    true
                }
            }

            true
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        val imageBadge = find<ImageView>(R.id.image_team)
        val nameTeam = find<TextView>(R.id.name_team)
        val formedYear = find<TextView>(R.id.formed_year)
        val stadiumName = find<TextView>(R.id.stadium)

//        called from FragmentTeamTab
        val intent = intent
        team = intent.getParcelableExtra<Team>("teamId")
        favoriteState()

        Picasso.get().load(team.teamBadge).into(imageBadge)
        overview = team.teamDescription.toString()
        nameTeam.text = team.teamName
        formedYear.text = team.teamFormedYear
        stadiumName.text = team.teamStadium

        idTeam = team.teamId.toString()


        viewpagerTeam.adapter = TeamPagerAdapter(supportFragmentManager)

        tabs_team.setupWithViewPager(viewpagerTeam)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(Team.TABLE_TEAM,
                        Team.ID_TEAM to team.teamId,
                        Team.TEAM_NAME to team.teamName,
                        Team.TEAM_BADGE to team.teamBadge,
                        Team.FORMED_YEAR to team.teamFormedYear,
                        Team.STADIUM_NAME to team.teamStadium,
                        Team.DESCRIPTION_EN to team.teamDescription)
            }
            snackbar(window.decorView.rootView,"Added to Favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(window.decorView.rootView, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(Team.TABLE_TEAM, "(ID_TEAM = {id})",
                        "id" to team.teamId.toString())
            }
            snackbar(window.decorView.rootView, "Removed to Favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(window.decorView.rootView, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    private fun favoriteState() {
        database.use {
            val result = select(Team.TABLE_TEAM)
                    .whereArgs("(ID_TEAM = {id})",
                            "id" to team.teamId.toString())
            val Team = result.parseList(classParser<Team>())
            if (!Team.isEmpty()) isFavorite = true
        }
    }
}
