package com.apps.rio.finalprojectkade.fragment.team


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.activity.TeamDetailActivity
import com.apps.rio.finalprojectkade.model.Team
import org.jetbrains.anko.find



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class OverviewFragment : Fragment() {
    private lateinit var team: Team
    companion object {
        fun newFragment(team: Team): OverviewFragment {
            val fragment = OverviewFragment()
            fragment.team = team

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_overview, container, false)
        val overview_team =  view.find<TextView>(R.id.overview_team)
        overview_team.text = TeamDetailActivity.overview


        return view
    }


}
