package com.apps.rio.finalprojectkade.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.R.id.*
import com.apps.rio.finalprojectkade.fragment.favorite.FavoriteTabs
import com.apps.rio.finalprojectkade.fragment.match.FragmentMatchTab
import com.apps.rio.finalprojectkade.fragment.team.FragmentTeamTab

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /*viewpager_match.adapter = MyPagerAdapter(supportFragmentManager)

        tabs_match.setupWithViewPager(viewpager_match)*/
        val bottom_navigation : BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                nav_match -> {
                    supportActionBar?.hide()
                    loadMatchFragment(savedInstanceState)
                }
                nav_team -> {
                    supportActionBar?.show()
                    loadTeamFragment(savedInstanceState)
                }
                nav_favorite -> {
                    supportActionBar?.hide()
                    loadFavoriteFragment(savedInstanceState)
                }
            }
            true
        }
        bottom_navigation.selectedItemId = nav_match

    }

    private fun loadMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content, FragmentMatchTab(), FragmentMatchTab::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadTeamFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content, FragmentTeamTab(), FragmentTeamTab::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadFavoriteFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content, FavoriteTabs(), FavoriteTabs::class.java.simpleName)
                    .commit()
        }
    }

}
