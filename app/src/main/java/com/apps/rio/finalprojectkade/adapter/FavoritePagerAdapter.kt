package com.apps.rio.finalprojectkade.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.apps.rio.finalprojectkade.fragment.favorite.FragmentFavorite
import com.apps.rio.finalprojectkade.fragment.favorite.TeamFavorite

class FavoritePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
           FragmentFavorite(),
            TeamFavorite()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position] as Fragment
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Match"
            else -> "Team"
        }
    }
}