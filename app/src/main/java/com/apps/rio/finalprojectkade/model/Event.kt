package com.apps.rio.finalprojectkade.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(

        val id: Long?,
        val idEvent: String?,
        val dateEvent: String?,
        val strTime: String?,
        val strSport :String?,
        // home team
        val idHomeTeam: String?,
        val strHomeTeam: String?,
        val intHomeScore: String?,
        val strHomeGoalDetails: String?,
        val intHomeShots: String?,
        val strHomeLineupGoalkeeper: String?,
        val strHomeLineupDefense: String?,
        val strHomeLineupMidfield: String?,
        val strHomeLineupForward: String?,
        val strHomeLineupSubstitutes: String?,

        // away team
        val idAwayTeam: String?,
        val strAwayTeam: String?,
        val intAwayScore: String?,
        val strAwayGoalDetails: String?,
        val intAwayShots: String?,
        val strAwayLineupGoalkeeper: String?,
        val strAwayLineupDefense: String?,
        val strAwayLineupMidfield: String?,
        val strAwayLineupForward: String?,
        val strAwayLineupSubstitutes: String?

) : Parcelable {
        companion object {
                const val TABLE_FAVORITE : String= "TABLE_FAVORITE"
                const val ID : String= "ID_"
                const val ID_EVENT : String= "ID_EVENT"
                const val DATE_EVENT: String = "DATE_EVENT"
                const val TIME_EVENT: String = "TIME_EVENT"
                const val SPORT_NAME: String = "SPORT_NAME"
                const val ID_TEAM_HOME: String = "ID_TEAM_HOME"
                const val HOME_TEAM: String = "HOME_TEAM"
                const val SCORE_HOME: String = "SCORE_HOME"
                const val HOME_GOAL: String = "HOME_GOAL"
                const val HOME_SHOT: String = "HOME_SHOT"
                const val HOME_KEEPER: String = "HOME_KEEPER"
                const val HOME_DEFENSE: String = "HOME_DEFENSE"
                const val HOME_MIDFIELD: String = "HOME_MIDFIELD"
                const val HOME_FORWARD: String = "HOME_FORWARD"
                const val HOME_SUBSTITUTE: String = "HOME_SUBSTITUTE"

                const val ID_TEAM_AWAY: String = "ID_TEAM_AWAY"
                const val AWAY_TEAM: String = "AWAY_TEAM"
                const val SCORE_AWAY: String = "SCORE_AWAY"
                const val AWAY_GOAL: String = "AWAY_GOAL"
                const val AWAY_SHOT: String = "AWAY_SHOT"
                const val AWAY_KEEPER: String = "AWAY_KEEPER"
                const val AWAY_DEFENSE: String = "AWAY_DEFENSE"
                const val AWAY_MIDFIELD: String = "AWAY_MIDFIELD"
                const val AWAY_FORWARD: String = "AWAY_FORWARD"
                const val AWAY_SUBSTITUTE: String = "AWAY_SUBSTITUTE"

        }
}


