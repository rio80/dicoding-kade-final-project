package com.apps.rio.finalprojectkade.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(
        var id: Long?,
        @SerializedName("idPlayer")
        var idPlayer: String? = null,
        @SerializedName("strPlayer")
        var playerName: String? = null,
        @SerializedName("strPosition")
        var playerPosition: String? = null,
        @SerializedName("strWeight")
        var playerWeight: String? = null,
        @SerializedName("strHeight")
        var playerHeight: String? = null,
        @SerializedName("strFanart1")
        var playerFanart1: String? = null,
        @SerializedName("strDescriptionEN")
        var descriptionEN: String? = null
        ) : Parcelable {
        companion object {
                const val TABLE_PLAYER : String= "TABLE_PLAYER"
                const val ID : String= "ID_"
                const val ID_PLAYER : String= "ID_PLAYER"
                const val PLAYER_NAME: String = "PLAYER_NAME"
                const val PLAYER_POSITION: String = "PLAYER_POSITION"
                const val PLAYER_WEIGHT: String = "PLAYER_WEIGHT"
                const val PLAYER_HEIGHT: String = "PLAYER_HEIGHT"
                const val FANART1: String = "FANART1"
        }
}

