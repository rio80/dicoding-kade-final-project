package com.apps.rio.finalprojectkade.view

import com.apps.rio.finalprojectkade.model.Team

interface TeamDetailView{
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}