package com.apps.rio.finalprojectkade.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import com.apps.rio.finalprojectkade.API.APIRepository
import com.apps.rio.finalprojectkade.R
import com.apps.rio.finalprojectkade.adapter.SearchMatchListAdapter
import com.apps.rio.finalprojectkade.model.Event
import com.apps.rio.finalprojectkade.presenter.SearchMatchPresenter
import com.apps.rio.finalprojectkade.view.SearchMatchView
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class SearchMatchActivity : AppCompatActivity(), SearchMatchView {
    override fun showLoading() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefresh.isRefreshing = false
    }

    override fun showList(data: List<Event>?) {
        hideLoading()

        events.clear()
        data?.forEach {
            if(it.strSport.equals("Soccer")){
                events.add(it)
            }
        }

//        events.addAll(data)
        adapter.notifyDataSetChanged()
    }


    private lateinit var searchView: SearchView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var matchList: RecyclerView
    private lateinit var presenter: SearchMatchPresenter
    private lateinit var adapter: SearchMatchListAdapter
    private var query: String = ""
    private var events: MutableList<Event> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val request = APIRepository()
        val gson = Gson()
        presenter = SearchMatchPresenter(this, request, gson)

        adapter = SearchMatchListAdapter(events){
            startActivity<DetailActivity>("position" to it)
        }

        linearLayout{
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light
                )

                matchList = recyclerView {
                    id = R.id.list_event
                    lparams(width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }.lparams(width = matchParent, height = matchParent)
        }

        matchList.adapter = adapter

        swipeRefresh.onRefresh {
            presenter.getList(query)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_view, menu)

        val searchItem = menu?.findItem(R.id.action_search)
        searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        searchView.setIconifiedByDefault(false)
        searchView.isIconified = false
        searchView.requestFocusFromTouch()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(text: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(text: String?): Boolean {
                if(text?.length!! > 2) {
                    query = text
                    presenter.getList(query)
                }
                return false
            }

        })

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }


    }
}
